﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgriculturalProducts
{
    /// <summary>
    /// This class for working with parent and child classes.
    /// </summary>
    public class WorkProducts
    {
        Products[] products;
        int amountProducts;
        /// <summary>
        /// Constructor for this class.
        /// </summary>
        public WorkProducts()
        {
            products = new Products[1000];
            amountProducts = 0;
        }
        /// <summary>
        /// This method adds new record in array.
        /// </summary>
        /// <param name="view">A string variable.</param>
        /// <param name="type">A string variable.</param>
        /// <param name="price">A double variable.</param>
        /// <param name="condition">A bool variable.</param>
        public void AddNewMemo(string view, string type, double price, bool condition)
        {
            if (type == "овощ")
            {
                products[amountProducts] = new Vegetables(view, price, condition);
            }
            else if (type == "фрукт")
            {
                products[amountProducts] = new Fruits(view, price, condition);
            }
            else
            {
                products[amountProducts] = new Berries(view, price, condition);
            }
            amountProducts++;
        }
        /// <summary>
        /// This method calculates average cost enviromental a certain kind products.
        /// </summary>
        /// <param name="view">A string variable.</param>
        /// <returns>Average cost.</returns>
        public string AverageCostEnvironmental(string view)
        {
            double cost = 0;
            int amount = 0;
            for (int i = 0; i < amountProducts; i++)
            {
                if ((products[i].Condition) && (products[i].View == view))
                {
                    cost += products[i].IncreaseCost();
                    amount++;
                }
            }
            return "Ср. цена экологической продукции: " + cost / (amount != 0 ? amount : 1) + "\n";
        }
        /// <summary>
        /// This method calculates average cost non-enviromental a certain kind products.
        /// </summary>
        /// <param name="view">A string variable.</param>
        /// <returns>Average cost.</returns>
        public string AverageCostNonEnvironmental(string view)
        {
            double cost = 0;
            int amount = 0;
            for (int i = 0; i < amountProducts; i++)
            {
                if ((!products[i].Condition) && (products[i].View == view))
                {
                    cost += products[i].IncreaseCost();
                    amount++;
                }
            }
            return "Ср. цена неэкологической продукции: " + cost / (amount != 0 ? amount : 1);
        }
        /// <summary>
        /// This method calculates quantity fruits, vegetables, berries.
        /// </summary>
        /// <returns>Quantity fruits, vegetables, berries</returns>
        public string NumberItems()
        {
            int fruit = 0;
            int vegetable = 0;
            int berry = 0;
            for (int i = 0; i < amountProducts; i++)
            {
                if ((!products[i].Condition))
                {
                    if (products[i] is Fruits)
                    {
                        fruit++;
                    }
                    else if (products[i] is Vegetables)
                    {
                        vegetable++;
                    }
                    else if (products[i] is Berries)
                    {
                        berry++;
                    }
                }
            }
            return "Кол-во фруктов: " + fruit + "\n" + "Кол-во овощей: " + vegetable + "\n" + "Кол-во ягод: " + berry + "\n";
        }
        /// <summary>
        /// Redefines ToString method to output all array records.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string answer = "";
            for (int i = 0; i < amountProducts; i++)
            {
                answer += products[i].ToString() + "\n";
            }
            return answer;
        }
    }
}
