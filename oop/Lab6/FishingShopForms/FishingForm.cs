using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using FishingShopLibrary;

namespace FishingShopForms
{
    public partial class FishingForm : Form
    {
        public FishingForm()
        {
            InitializeComponent();
        }
        FishShop fish = new FishShop();
        //This method return array of index purchases.
        private int[] CheckedListBox1_SelectedIndexChanged()
        {
            //checkedListBox1.SelectionMode = SelectionMode.MultiExtended;
            int[] indexGaer = new int[checkedListBox1.SelectedIndices.Count];
            for (int i = 0; i < checkedListBox1.SelectedIndices.Count; i++)
            {
                indexGaer[i] = checkedListBox1.SelectedIndices[i];
            }
            return indexGaer;
        }
        private void Button4_Click(object sender, EventArgs e)
        {
            if ((dateTimePicker3.Checked) && (listBox1.SelectedItems.Count > 0) && (checkedListBox1.SelectedItems.Count > 0))
            {
                fish.AddNewMemo(CheckedListBox1_SelectedIndexChanged(), ListBox1_SelectedIndexChanged(), DateTimePicker3_ValueChanged());
                for (int i = 0; i < 7; i++)
                {
                    checkedListBox1.SetItemCheckState(i, CheckState.Unchecked);
                }
                checkedListBox1.Update();
                listBox1.SetSelected(listBox1.SelectedIndex, false);
            }
            else
            {
                textBox1.Text = ("�� ����� �� ��� ��������!");
            }
        }
        //This method return condition fishing.
        private bool ListBox1_SelectedIndexChanged()
        {
            bool condition = false;
            if (listBox1.SelectedIndex == 0)
            {
                condition = false;
            }
            else if (listBox1.SelectedIndex == 1)
            {
                condition = true;
            }
            return condition;
        }
        //This method return date purchase.
        private DateTime DateTimePicker3_ValueChanged()
        {
            DateTime date;
            date = dateTimePicker3.Value;
            return date;
        }
        //This method return date start of period.
        private DateTime DateTimePicker1_ValueChanged()
        {
            DateTime date;
            date = dateTimePicker1.Value;
            return date;
        }
        //This method return date finish of period.
        private DateTime DateTimePicker2_ValueChanged()
        {
            DateTime date;
            date = dateTimePicker2.Value;
            return date;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = ("�������� ������!");
            if ((dateTimePicker1.Checked) && (dateTimePicker2.Checked))
            {
                textBox1.Text = (fish.AverageAmount(DateTimePicker1_ValueChanged(), DateTimePicker2_ValueChanged()).ToString());
            }
        }
        private void Button2_Click(object sender, EventArgs e)
        {
            if ((dateTimePicker1.Checked) && (dateTimePicker2.Checked) && (listBox1.SelectedItems.Count > 0))
                textBox1.Text = (fish.VolumeSales(DateTimePicker1_ValueChanged(), DateTimePicker2_ValueChanged(), ListBox1_SelectedIndexChanged()).ToString());
            else
                textBox1.Text = ("�� ����� �� ��� ���������!");
        }
        private void Button3_Click(object sender, EventArgs e)
        {
            int[] indexArray;
            indexArray = fish.PopularGood(DateTimePicker1_ValueChanged(), DateTimePicker2_ValueChanged());
            if ((dateTimePicker1.Checked) && (dateTimePicker2.Checked))
                textBox1.Text = (fish.PopularGoods(indexArray));
            else
                textBox1.Text = ("�� ����� �� ��� ���������!");
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            textBox1.Text = fish.ToString();
        }

        private void FishingForm_Load(object sender, EventArgs e)
        {

        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}