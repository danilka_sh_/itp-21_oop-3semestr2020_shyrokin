﻿using System;

namespace FishingShopLibrary
{
    /// <summary>
    /// This class work with subclass and methods associated with it.
    /// </summary>
    public class FishShop
    {
        private class Purchase
        {
            private enum FishingGear
            {
                rod = 177,
                spinning = 150,
                feeder = 136,
                fishLine = 119,
                wobbler = 514,
                mormyshka = 52,
                reel = 542
            };
        // 0 - winter, 1 - summer
            public bool fishCondition; 
            public DateTime date;
            public int[] index;
            public Purchase(int[] indexFisingGaer, bool fishCondition, DateTime date)
            {
                this.fishCondition = fishCondition;
                this.date = date;
                index = indexFisingGaer;
            }
            public int CalcCost()
            {
                int cost = 0;
                for (int i = 0; i < index.Length; i++)
                {
                    switch (index[i])
                    {
                        case 0:
                            cost += (int)FishingGear.rod;
                            break;
                        case 1:
                            cost += (int)FishingGear.spinning;
                            break;
                        case 2:
                            cost += (int)FishingGear.feeder;
                            break;
                        case 3:
                            cost += (int)FishingGear.fishLine;
                            break;
                        case 4:
                            cost += (int)FishingGear.wobbler;
                            break;
                        case 5:
                            cost += (int)FishingGear.mormyshka;
                            break;
                        case 6:
                            cost += (int)FishingGear.reel;
                            break;
                    }
                }
                return cost;
            }
        }
        private Purchase[] sellings;
        int amountSellings;
        /// <summary>
        /// It's a constructor for class FishShop.
        /// </summary>
        public FishShop()
        {
            sellings = new Purchase[1000];
            amountSellings = 0;
        }
        /// <summary>
        /// This method add new purchase.
        /// </summary>
        /// <param name="indexFisingGaer">It's int array index of purchase's price.</param>
        /// <param name="period">Bool variable which shows the catch condition.</param>
        /// <param name="date">It's DateTime variable which shows the date of purchase.</param>
        public void AddNewMemo(int[] indexFisingGaer, bool period, DateTime date)
        {
            sellings[amountSellings] = new Purchase(indexFisingGaer, period, date);
            amountSellings++;
        }
        /// <summary>
        /// This method calculations average cost of purchases.
        /// </summary>
        /// <param name="beginningPeriod">DateTime variable.</param>
        /// <param name="endPeriod">DateTime variable.</param>
        /// <returns></returns>
        public int AverageAmount(DateTime beginningPeriod, DateTime endPeriod)
        {
            int averaeCost = 0;
            int amountPurchese = 0;
            for(int i = 0; i < amountSellings; i++)
            {
                if ((sellings[i].date >= beginningPeriod) && (sellings[i].date <= endPeriod))
                {
                    averaeCost += sellings[i].CalcCost();
                    amountPurchese++;
                }
            }
            return averaeCost / (amountPurchese != 0 ? amountPurchese : 1);
        }
        /// <summary>
        /// This method calculates sales.
        /// </summary>
        /// <param name="beginningPeriod">DateTime variable.</param>
        /// <param name="endPeriod">DateTime variable.</param>
        /// <param name="fishCondition">Bool variable which shows the catch condition.</param>
        /// <returns></returns>
        public int VolumeSales(DateTime beginningPeriod, DateTime endPeriod, bool fishCondition)
        {
            int volumeCost = 0;
            for (int i = 0; i < amountSellings; i++)
            {
                if (fishCondition == sellings[i].fishCondition)
                {
                    if ((sellings[i].date >= beginningPeriod) && (sellings[i].date <= endPeriod))
                    {
                        volumeCost += sellings[i].CalcCost();
                    }
                }
            }
            return volumeCost;
        }
        /// <summary>
        /// Depending on the index, this method displays the goods.
        /// </summary>
        /// <param name="number">The int index.</param>
        /// <returns>The string with popular product.</returns>
        private string FishingGoods(int number)
        {
            string popularGood = "";
            switch (number)
            {
                case 0:
                    popularGood = "удочка";
                    break;
                case 1:
                    popularGood = "спиннинг";
                    break;
                case 2:
                    popularGood = "фидер";
                    break;
                case 3:
                    popularGood = "леска";
                    break;
                case 4:
                    popularGood = "воблер";
                    break;
                case 5:
                    popularGood = "мормышка";
                    break;
                case 6:
                    popularGood = "катушка";
                    break;
            }
            return popularGood;
        }
        /// <summary>
        /// This method searches the number of calls to each product.
        /// </summary>
        /// <param name="beginningPeriod">DateTime variable.</param>
        /// <param name="endPeriod">DateTime variable.</param>
        /// <returns>The array with the number of calls to each product.</returns>
        public int[] PopularGood(DateTime beginningPeriod, DateTime endPeriod)
        {
            //string popularGood = "";
            int[] amount = new int[7];
            for (int i = 0; i < amountSellings; i++)
            {
                if ((sellings[i].date >= beginningPeriod) && (sellings[i].date <= endPeriod))
                {
                    for (int j = 0; j < (sellings[i].index).Length; j++)
                        amount[sellings[i].index[j]]++;
                }
            }
            return amount;
        }
        /// <summary>
        /// This method searches the most popular product. 
        /// </summary>
        /// <param name="amount">Int array variable.</param>
        /// <returns>The string with the most popular product.</returns>
        public string PopularGoods(int[] amount)
        {
            int max = 0;
            int maxIndex = 0;
            string popularGood = "";
            bool means = false;
            for (int i = 0; i < 7; i++)
            {
                bool mean = true;
                for (int m = 0; m < 7; m++)
                {
                    if ((amount[m] == amount[i]) && (i != m))
                        mean = false;
                }
                if (max < amount[i])
                {
                    max = amount[i];
                    maxIndex = i;
                    means = mean;
                }
            }
            if(means)
                popularGood = "Самый популярный товар: " + FishingGoods(maxIndex);
            else
                popularGood = "Нет самого популярного товара.";
            return popularGood;
        }
        /// <summary>
        /// This method return a string dspending on fishing conditions.
        /// </summary>
        /// <param name="index">Number of element.</param>
        /// <returns>A string.</returns>
        private string FishConditionToString(int index)
        {
            string condition = "условие ловли - ";
            if(sellings[index].fishCondition)
            {
                condition += "лето";
            }
            else
            {
                condition += "зима";
            }
            return condition;
        }
        /// <summary>
        /// This method forms a string of purchases.
        /// </summary>
        /// <returns>A list of purchases.</returns>
        public string ToString()
        {
            string buy = "";
            for(int i = 0; i < amountSellings; i++)
            {
                buy += i + 1+ ". " + FishConditionToString(i) + " дата покупки: " + sellings[i].date + "\n";
                buy += "\n" + "     Покупки: \n";
                for (int j = 0; j < (sellings[i].index).Length; j++)
                    buy += FishingGoods(sellings[i].index[j]) + "\n";
            }
            return buy;
        }
    }
}
