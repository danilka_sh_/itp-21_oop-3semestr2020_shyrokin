﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FishingShopLibrary;

namespace FishingGearUnitTest
{
    [TestClass]
    public class FishTest
    {
        [TestMethod]
        public void AverageAmount1()
        {
            //Arrange
            FishShop fish = new FishShop();
            DateTime date1 = new DateTime(2019, 11, 2);
            DateTime date2 = new DateTime(2019, 11, 10);
            DateTime dateStart = new DateTime(2019, 11, 1);
            DateTime dateFinish = new DateTime(2019, 11, 15);
            int[] indexFisingGear1 = { 0, 1, 2 };
            int[] indexFisingGear2 = { 1, 3, 4 };
            fish.AddNewMemo(indexFisingGear1, false, date1);
            fish.AddNewMemo(indexFisingGear2, false, date2);
            //Act
            int assert = 623;
            //Assert
            Assert.AreEqual(fish.AverageAmount(dateStart, dateFinish), assert);
        }
        [TestMethod]
        public void AverageAmount2()
        {
            //Arrange
            FishShop fish = new FishShop();
            DateTime date1 = new DateTime(2019, 10, 2);
            DateTime date2 = new DateTime(2019, 11, 10);
            DateTime dateStart = new DateTime(2019, 11, 3);
            DateTime dateFinish = new DateTime(2019, 11, 16);
            int[] indexFisingGear1 = { 4, 5, 6 };
            int[] indexFisingGear2 = { 0, 1, 6 };
            fish.AddNewMemo(indexFisingGear1, false, date1);
            fish.AddNewMemo(indexFisingGear2, false, date2);
            //Act
            int assert = 869;
            //Assert
            Assert.AreEqual(fish.AverageAmount(dateStart, dateFinish), assert);
        }
        [TestMethod]
        public void AverageAmount3()
        {
            //Arrange
            FishShop fish = new FishShop();
            DateTime date1 = new DateTime(2019, 9, 2);
            DateTime date2 = new DateTime(2019, 10, 12);
            DateTime date3 = new DateTime(2019, 11, 14);
            DateTime dateStart = new DateTime(2019, 8, 1);
            DateTime dateFinish = new DateTime(2019, 10, 15);
            int[] indexFisingGear1 = { 0, 1, 2 };
            int[] indexFisingGear2 = { 1, 3, 4 };
            int[] indexFisingGear3 = { 4, 5, 6 };
            fish.AddNewMemo(indexFisingGear1, false, date1);
            fish.AddNewMemo(indexFisingGear2, false, date2);
            fish.AddNewMemo(indexFisingGear3, false, date3);
            //Act
            int assert = 623;
            //Assert
            Assert.AreEqual(fish.AverageAmount(dateStart, dateFinish), assert);
        }
        [TestMethod]
        public void VolumeSales1()
        {
            //Arrange
            FishShop fish = new FishShop();
            DateTime date1 = new DateTime(2019, 11, 2);
            DateTime date2 = new DateTime(2019, 11, 12);
            DateTime date3 = new DateTime(2019, 11, 14);
            DateTime dateStart = new DateTime(2019, 9, 1);
            DateTime dateFinish = new DateTime(2019, 11, 15);
            int[] indexFisingGear1 = { 0, 1, 2 };
            int[] indexFisingGear2 = { 1, 3, 4 };
            int[] indexFisingGear3 = { 4, 5, 6 };
            fish.AddNewMemo(indexFisingGear1, false, date1);
            fish.AddNewMemo(indexFisingGear2, true, date2);
            fish.AddNewMemo(indexFisingGear3, false, date3);
            //Act
            int assert = 1571;
            //Assert
            Assert.AreEqual(fish.VolumeSales(dateStart, dateFinish, false), assert);
        }
        [TestMethod]
        public void VolumeSales2()
        {
            //Arrange
            FishShop fish = new FishShop();
            DateTime date1 = new DateTime(2019, 9, 2);
            DateTime date2 = new DateTime(2019, 10, 12);
            DateTime dateStart = new DateTime(2019, 8, 1);
            DateTime dateFinish = new DateTime(2019, 10, 15);
            int[] indexFisingGear1 = { 0, 1, 2 };
            int[] indexFisingGear2 = { 1, 3, 4 };
            fish.AddNewMemo(indexFisingGear1, true, date1);
            fish.AddNewMemo(indexFisingGear2, false, date2);
            //Act
            int assert = 463;
            //Assert
            Assert.AreEqual(fish.VolumeSales(dateStart, dateFinish, true), assert);
        }
        [TestMethod]
        public void PopularGood1()
        {
            //Arrange
            FishShop fish = new FishShop();
            DateTime date1 = new DateTime(2019, 9, 2);
            DateTime date2 = new DateTime(2019, 10, 12);
            DateTime dateStart = new DateTime(2019, 8, 1);
            DateTime dateFinish = new DateTime(2019, 10, 15);
            int[] indexFisingGear1 = { 0, 1, 2 };
            int[] indexFisingGear2 = { 1, 3, 4 };
            fish.AddNewMemo(indexFisingGear1, true, date1);
            fish.AddNewMemo(indexFisingGear2, false, date2);
            int[] index;
            index = fish.PopularGood(dateStart, dateFinish);
            //Act
            string assert = "Самый популярный товар: " + "спиннинг";
            //Assert
            Assert.AreEqual(fish.PopularGoods(index), assert);
        }
        [TestMethod]
        public void PopularGood2()
        {
            //Arrange
            FishShop fish = new FishShop();
            DateTime date1 = new DateTime(2019, 10, 2);
            DateTime date2 = new DateTime(2019, 10, 12);
            DateTime dateStart = new DateTime(2019, 8, 1);
            DateTime dateFinish = new DateTime(2019, 10, 15);
            int[] indexFisingGear1 = { 0, 1, 5 };
            int[] indexFisingGear2 = { 2, 3, 4 };
            fish.AddNewMemo(indexFisingGear1, true, date1);
            fish.AddNewMemo(indexFisingGear2, false, date2);
            int[] index;
            index = fish.PopularGood(dateStart, dateFinish);
            //Act
            string assert = "Нет самого популярного товара.";
            //Assert
            Assert.AreEqual(fish.PopularGoods(index), assert);
        }
    }
}
