﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextWord
{
    /// <summary>
    /// Класс, читает файл
    /// </summary>
    public class ReadFile
    {
        public string[] Read()
        {
            string path = "Numbers.txt";
            string res;
            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    res = sr.ReadToEnd();
                    return res.Split('\n');
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return new string[0];
        }

    }
}
