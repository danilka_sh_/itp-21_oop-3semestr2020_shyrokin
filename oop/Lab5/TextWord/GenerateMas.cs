﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TextWord
{
	public class GenerateMas
	{
		private string[] mas;
		/// <summary>
		/// Инициализация mas
		/// </summary>
		/// <param name="stroks">Параметры строк</param>
		public GenerateMas(string[] stroks)
		{
			mas = new string[stroks.Length];
			for (int i = 0; i < stroks.Length; i++)
				mas[i] = stroks[i];
		}
		/// <summary>
		/// Создание и добавление массива
		/// </summary>
		/// <param name="pattern"></param>
		/// <param name="len"></param>
		/// <returns></returns>
		private string[] CreateMas(Regex pattern, int len)
		{
			List<string> numb = new List<string>();
			bool flag = true;

			for (int i = 0; i < mas.Length; i++)
			{
				if (pattern.IsMatch(mas[i]) && len == mas[i].Length)
				{
					flag = true;
					for (int j = 0; j < numb.Count; j++)
						if (mas[i] != null && numb[j] != null && numb[j].Equals(mas[i])) flag = false;
					if (flag)
						numb.Add(mas[i]);
				}
			}

			return numb.ToArray();
		}
		public string[] NumberFull()
		{
			Regex pattern1 = new Regex(@"\d{3}-\d{2}-\d{3}-\d{2}-\d{2}");
			return CreateMas(pattern1, 17);
		}

		public string[] NumberOffCode()
		{
			Regex pattern2 = new Regex(@"\d{2}-\d{3}-\d{2}-\d{2}");
			return CreateMas(pattern2, 13);
		}

		public string[] NumberOffCodeOperator()
		{
			Regex pattern3 = new Regex(@"\d{3}-\d{2}-\d{2}");
			return CreateMas(pattern3, 10);
		}

		public override string ToString()
		{
			return mas.ToString();
		}
		/// <summary>
		/// Проверка строк на ошибки
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public override bool Equals(object obj)
		{
			if (GetType() != obj.GetType())
				return false;
			GenerateMas stroks = (GenerateMas)obj;
			if (mas.Length != stroks.mas.Length)
				return false;
			for (int i = 0; i < mas.Length; i++)
				if (mas[i] != stroks.mas[i])
					return false;
			return true;
		}
		/// <summary>
		/// Хэш - код
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode()
		{
			return mas.Length;
		}

	}
}
