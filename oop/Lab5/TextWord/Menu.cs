﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextWord
{
	public class Menu
	{
		/// <summary>
		/// Меню
		/// </summary>
		public void MyMenu()
		{
			ReadFile readF = new ReadFile();
			GenerateMas big_mas = new GenerateMas(readF.Read());

			Console.WriteLine("Полные номера: ");

			string[] full = big_mas.NumberFull();
			foreach (string s in full)
				Console.WriteLine(s);

			Console.WriteLine("\nНомера без кода страны: ");

			string[] full1 = big_mas.NumberOffCode();
			foreach (string s in full1)
				Console.WriteLine(s);

			Console.WriteLine("\nНомера без кода страны и оператора: ");

			string[] full2 = big_mas.NumberOffCodeOperator();
			foreach (string s in full2)
				Console.WriteLine(s);

			Console.ReadLine();
		}
	}
}
