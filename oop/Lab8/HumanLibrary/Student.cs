﻿using System;
using System.Text.RegularExpressions;

namespace HumanLibrary
{
    /// <summary>
    /// It's child class of Human.
    /// </summary>
    public class Student : Human
    {
        /// <summary>
        /// It's property responsible for the array of student grades.
        /// </summary>
        public int[] Grades { get; private set; }
        /// <summary>
        /// It's overrided constructor for a child class Student.
        /// </summary>
        /// <param name="surname">It's string variable.</param>
        /// <param name="yearBirth">It's int variable.</param>
        /// <param name="stat">It's string variable.</param>
        /// <param name="grades">It's int array.</param>
        public Student(string surname, int yearBirth, string stat, int[] grades)
            : base(surname, yearBirth, stat)
        {
            Grades = grades;
        }
        /// <summary>
        /// It's overrided method which counts average score per session.
        /// </summary>
        /// <returns>Total annual load.</returns>
        public override int Svedenija()
        {
            int average = 0;
            for (int i = 0; i < Grades.Length; i++)
            {
                average += Grades[i];
            }
            return average / Grades.Length;
        }
        /// <summary>
        /// This method form class Student. 
        /// </summary>
        /// <param name="text">It's string variable.</param>
        /// <param name="s">It's Student variable.</param>
        /// <returns>True if class create and false if not.</returns>
        public static bool CheckFotmattString(string text,out Student s)
        {
            string[] words;
            string surnames = "";
            int year = 0;
            int count = 0;
            int[] grade = new int[4];
            Regex surname = new Regex(@"[А-Я][а-я]+");
            words = text.Split(' ');
            if(words[2] == "студент")
            {
                for (int j = 0; j < words.Length; j++)
                {
                    surnames = words[0];
                    Int32.TryParse(words[1], out int result);
                    year = result;
                    if (Int32.TryParse(words[j], out int result1) && result1 <= 10)
                    {
                        grade[count] = result1;
                        count++;
                    }
                }
                s = new Student(surnames, year, "студент", grade);
                return true;
            }
            else
            {
                s = null;
                return false;
            }
        }
    }
}
