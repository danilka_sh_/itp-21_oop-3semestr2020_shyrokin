﻿using System;
using System.Text.RegularExpressions;

namespace HumanLibrary
{
    /// <summary>
    /// It's class for work with parent and child classes.
    /// </summary>
    public class WorkHuman
    {
        Human[] human;
        /// <summary>
        /// It's property responsible for the length of array of human.
        /// </summary>
        public int HumanLength { get; private set; }
        /// <summary>
        /// It's class constructor.
        /// </summary>
        public WorkHuman()
        {
            human = new Human[1000];
            HumanLength = 0;
        }
        /// <summary>
        /// This method forms array of objects.
        /// </summary>
        /// <param name="text">It's string variable.</param>
        /// <returns>A Human array.</returns>
        public Human[] WordParser(string text)
        {
            string[] separator = { "\r\n" };
            string[] sentences = text.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            Student s;
            Teacher t;
            Human h;
            HumanLength = sentences.Length;
            for (int i = 0; i < sentences.Length; i++)
            {
                HumanLength = sentences.Length;
                if (Student.CheckFotmattString(sentences[i], out s))
                    human[i] = s;
                else if (Teacher.CheckFotmattString(sentences[i], out t))
                    human[i] = t; 
                else if (Human.CheckFotmattString(sentences[i], out h))
                    human[i] = h;  
            }
            return human;
        }
        /// <summary>
        /// This method considers the average load of theachers older than years in a particular subject.
        /// </summary>
        /// <param name="subject">It's string variable.</param>
        /// <returns>The average load of theachers.</returns>
        public int MaxLoad(string subject)
        {
            int maxLoad = 0;
            for (int i = 0; i < HumanLength; i++)
            {
                if ((2019 - human[i].YearBirth > 40) && (human[i] is Teacher))
                {
                    Teacher teacher = human[i] as Teacher;
                    maxLoad += teacher[subject];
                }
            }
            return maxLoad;
        }
        /// <summary>
        /// This method looking for students over 19 with more than one 9.
        /// </summary>
        /// <returns>An array of students with more than one 9</returns>
        public string[] InfoStudent()
        {
            int amount = 0;
            string[] stringStudent = new string[HumanLength];
            for (int i = 0; i < HumanLength; i++)
            {
                if ((human[i] is Student))
                {
                    Student student = human[i] as Student;
                    for (int j = 0; j < (student.Grades).Length; j++)
                    {
                        if (student.Grades[j] == 9)
                        {
                            amount++;
                        }
                    }
                    if (amount > 1)
                    {
                        stringStudent[i] = student.ToString();
                    }
                    amount = 0;
                }
            }
            return stringStudent;
        }
        /// <summary>
        /// This method forms array of string. 
        /// </summary>
        /// <returns>Information about human.</returns>
        public string[] FormString()
        {
            string[] answer = new string[HumanLength];
            for (int i = 0; i < HumanLength; i++)
            {
                answer[i] = human[i].ToString() + "\r\n";
            }
            return answer;
        }
        /// <summary>
        /// Sorts an array of people by last name.
        /// </summary>
        public void SortSurname()
        {
            Array.Sort(human, 0, HumanLength);
        }
        /// <summary>
        /// This method looking for students over 19 years old.
        /// </summary>
        /// <param name="index">It's array of bool variables.</param>
        /// <returns>An array of string.</returns>
        public string[] StudentRed(out bool[] index)
        {
            index = new bool[HumanLength];
            string[] humanString = new string[HumanLength];
            for (int i = 0; i < HumanLength; i++)
            {
                if ((human[i] is Student) && (2019 - human[i].YearBirth > 19))
                {
                    index[i] = true;
                }
                humanString[i] = human[i].ToString();
            }
            return humanString;
        }
    }
}
