﻿using System;
using System.Text.RegularExpressions;

namespace HumanLibrary
{
    /// <summary>
    /// It's parent class of people with implemented interface.
    /// </summary>
    public class Human : IComparable<Human>
    {
        string surname;
        /// <summary>
        /// It's property responsible for the year of birth.
        /// </summary>
        public int YearBirth { get; private set; }
        string stat;
        /// <summary>
        /// It's class constructor.
        /// </summary>
        /// <param name="surname">It's string variable.</param>
        /// <param name="yearBirth">It's int variable.</param>
        /// <param name="stat">It's string variable.</param>
        public Human(string surname, int yearBirth, string stat)
        {
            this.surname = surname;
            YearBirth = yearBirth;
            this.stat = stat;
        }
        /// <summary>
        /// This method which counts age of person.
        /// </summary>
        /// <returns>Age of person.</returns>
        public virtual int Svedenija()
        {
            return 2019 - YearBirth;
        }
        /// <summary>
        /// Method forming a string.
        /// </summary>
        /// <returns>Information about people.</returns>
        public override string ToString()
        {
            return "Фамилия: " + surname + "; статус: " + stat + "; год рождения: " + YearBirth + "; сведения: " + Svedenija() + "\n";
        }
        /// <summary>
        /// This method is designed to compare the current object with the object that is passed as a parameter.
        /// </summary>
        /// <param name="h">It's object of Human class.</param>
        /// <remarks>The value is less than zero if the current object is before the parameter object, zero if both objects are equal and greater than zero if after the parameter object.</remarks>
        /// <returns>An int variable.</returns>
        public int CompareTo(Human h)
        {
            return surname.CompareTo(h.surname);
        }
        /// <summary>
        /// This method form class Human. 
        /// </summary>
        /// <param name="text">It's string variable.</param>
        /// <param name="h">It's Human variable.</param>
        /// <returns>True if class create and false if not.</returns>
        public static bool CheckFotmattString(string text, out Human h)
        {
            string[] words;
            string surnames = "";
            int year = 0;
            string stat = "";
            words = text.Split(' ');
            if(words[2] != "преподаватель" && words[2] != "студент")
            {
                for (int j = 0; j < words.Length; j++)
                {
                    surnames = words[0];
                    Int32.TryParse(words[1], out int result);
                    year = result;
                    stat = words[2];
                }
                h = new Human(surnames, year, stat);
                return true;
            }
            else
            {
                h = null;
                return false;
            }
        }
    }
}
