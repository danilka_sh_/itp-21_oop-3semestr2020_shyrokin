﻿using System;
using System.Text.RegularExpressions;

namespace HumanLibrary
{
    /// <summary>
    /// It's child class of Human.
    /// </summary>
    public class Teacher : Human
    {
        /// <summary>
        /// It's property responsible for the array of load teacher.
        /// </summary>
        public int[] Load { get; private set; }
        /// <summary>
        /// It's overrided constructor for a child class Teacher.
        /// </summary>
        /// <param name="surname">It's string variable.</param>
        /// <param name="yearBirth">It's int variable.</param>
        /// <param name="stat">It's string variable.</param>
        /// <param name="load">It's int array.</param>
        public Teacher(string surname, int yearBirth, string stat, int[] load)
            : base(surname, yearBirth, stat)
        {
            Load = load;
        }
        /// <summary>
        /// It's indexer with string index type.
        /// </summary>
        /// <param name="index">It's string variable.</param>
        /// <returns>DEpending on the index returns an element of the array.</returns>
        public int this[string index]
        {
            get
            {
                switch (index)
                {
                    case "ОАиП": return Load[0];
                    case "Математика": return Load[1];
                    case "Физика": return Load[2];
                    default: return 0;
                }
            }
            set
            {
                switch (index)
                {
                    case "ОАиП":
                        Load[0] = value;
                        break;
                    case "Математика":
                        Load[1] = value;
                        break;
                    case "Физика":
                        Load[2] = value;
                        break;
                }
            }
        }
        /// <summary>
        /// It's overrided method which counts load of teacher.
        /// </summary>
        /// <returns>Total annual load.</returns>
        public override int Svedenija()
        {
            int average = 0;
            for (int i = 0; i < Load.Length; i++)
            {
                average += Load[i];
            }
            return average;
        }
        /// <summary>
        /// This method form class Teacher. 
        /// </summary>
        /// <param name="text">It's string variable.</param>
        /// <param name="t">It's Teacher variable.</param>
        /// <returns>True if class create and false if not.</returns>
        public static bool CheckFotmattString(string text, out Teacher t)
        {
            string[] words;
            string surnames = "";
            int year = 0;
            int count = 0;
            int[] load = new int[3];
            Regex surname = new Regex(@"[А-Я][а-я]+");
            words = text.Split(' ');
            if (words[2] == "преподаватель")
            {
                for (int j = 0; j < words.Length; j++)
                {
                    surnames = words[0];
                    Int32.TryParse(words[1], out int result);
                    year = result;
                    if (Int32.TryParse(words[j], out int result2) && result2 > 10 && result2 < 200)
                    {
                        load[count] = result2;
                        count++;
                    }
                }
                t = new Teacher(surnames, year, "преподаватель", load);
                return true;
            }
            else
            {
                t = null;
                return false;
            }
        }
    }
}
