﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Examen;

namespace lab_2
{
	/// <summary>
	/// Начальный класс
	/// </summary>
	class Program
	{
		/// <summary>
		/// Точка входа программы
		/// </summary>
		/// <param name="args">Аргументы метода Main</param>
		static void Main(string[] args)
		{
			Menu menu = new Menu();
			menu.Show();
		}
	}
}
