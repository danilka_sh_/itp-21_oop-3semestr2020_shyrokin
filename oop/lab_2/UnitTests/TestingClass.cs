﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Examenation;

namespace UnitTests
{
	/// <summary>
	/// Класс для тестирование типа Examenation
	/// </summary>
	[TestClass]
	public class TestingClass
	{
		/// <summary>
		/// Тестирование перегруженного оператора +
		/// </summary>
		[TestMethod]
		public void TestingOverloadOperationPlus()
		{
			Examenation Ex1 = new Examenation("Данила", "Широкин", "Евгеньевич", 7);
			Examenation Ex2 = new Examenation("Данила", "Широкин", "Евгеньевич", 9);

			var expected = new Examenation("Данила", "Широкин", "Евгеньевич", 8);
			var actual = Ex1 + Ex2;

			Assert.AreEqual(expected, actual);
		}

		/// <summary>
		/// Тестирование перегруженного оператора +
		/// </summary>
		[TestMethod]
		public void TestingOverloadOperationPlusThreeExam()
		{
			Examenation Ex1 = new Examenation("Данила", "Широкин", "Евгеньевич", 7);
			Examenation Ex2 = new Examenation("Данила", "Широкин", "Евгеньевич", 9);
			Examenation Ex3 = new Examenation("Данила", "Широкин", "Евгеньевич", 4);

			var expected = new Examenation("Данила", "Широкин", "Евгеньевич", 6);
			var sum = Ex1 + Ex2;
			var actual = sum + Ex3;

			Assert.AreEqual(expected, actual);
		}

		/// <summary>
		/// Тестирование перегруженного оператора -
		/// </summary>
		[TestMethod]
		public void TestingOverloadOperationMinus()
		{
			Examenation Ex1 = new Examenation("Данила", "Широкин", "Евгеньевич", 10);
			Examenation Ex2 = new Examenation("Данила", "Широкин", "Евгеньевич", 8);

			var expected = new Examenation("Данила", "Широкин", "Евгеньевич", 9);
			var actual = Ex1 - Ex2;

			Assert.AreEqual(expected, actual);
		}

		/// <summary>
		/// Тестирование перегруженных операторов + и - 
		/// </summary>
		[TestMethod]
		public void TestingOverloadOperationMixMinusAndPlus()
		{
			Examenation Ex1 = new Examenation("Данила", "Широкин", "Евгеньевич", 10);
			Examenation Ex2 = new Examenation("Данила", "Широкин", "Евгеньевич", 8);
			Examenation Ex3 = new Examenation("Данила", "Широкин", "Евгеньевич", 4);

			var expected = new Examenation("Данила", "Широкин", "Евгеньевич", 6.5);
			var sum = Ex1 + Ex2;
			var actual = sum - Ex3; ;

			Assert.AreEqual(expected, actual);
		}

		/// <summary>
		/// Тестирование перегруженных операторов + и - 
		/// </summary>
		[TestMethod]
		public void TestingOverloadOperationPlusThrowMinus()
		{
			Examenation Ex1 = new Examenation("Данила", "Широкин", "Евгеньевич", 10);
			Examenation Ex2 = new Examenation("Данила", "Широкин", "Евгеньевич", 8);

			var expected = Ex1 + Ex2;
			var actual = Ex2 - Ex1; ;

			Assert.AreEqual(expected, actual);
		}

		/// <summary>
		/// Тестирование метода ToString()
		/// </summary>
		[TestMethod]
		public void TestingToString()
		{
			Examenation Ex = new Examenation("Даниил", "Широкин", "Евгеньевич", 10);

			string expected = "ФИО студента: Широкин Даниил Евгеньевич\nСредняя оценка за все экзамены = 10";
			var actual = Ex.ToString();

			Assert.AreEqual(expected, actual);
		}

		/// <summary>
		/// Тестирование метода ToString()
		/// </summary>
		[TestMethod]
		public void TestingToStringTwoObject()
		{
			Examenation Ex1 = new Examenation("Даниил", "Широкин", "Евгеньевич", 10);
			Examenation Ex2 = new Examenation("Даниил", "Широкин", "Евгеньевич", 4);
			Examenation res = Ex1 + Ex2;
			string expected = "ФИО студента: Широкин Даниил Евгеньевич\nСредняя оценка за все экзамены = 7";
			var actual = res.ToString();

			Assert.AreEqual(expected, actual);
		}

		/// <summary>
		/// Тестирование метода Equals()
		/// </summary>
		[TestMethod]
		public void TestingEqualsSameObject()
		{
			Examenation Ex1 = new Examenation("Даниил", "Широкин", "Евгеньевич", 10);
			Examenation Ex2 = new Examenation("Даниил", "Широкин", "Евгеньевич", 10);
			bool expected = true;
			var actual = Ex1.Equals(Ex2);

			Assert.AreEqual(expected, actual);
		}

		/// <summary>
		/// Тестирование метода Equals()
		/// </summary>
		[TestMethod]
		public void TestingEqualsDifferentObject()
		{
			Examenation Ex1 = new Examenation("Даниил", "Широкин", "Евгеньевич", 10);
			Examenation Ex2 = new Examenation("Владислав", "Отчик", "Витальевич", 9);
			bool expected = false;
			var actual = Ex1.Equals(Ex2);

			Assert.AreEqual(expected, actual);
		}

		/// <summary>
		/// Тестирование метода Equals()
		/// </summary>
		[TestMethod]
		public void TestingEqualsNull()
		{
			Examenation Ex1 = new Examenation("Даниил", "Широкин", "Евгеньевич", 10);
			Examenation Ex2 = null;
			bool expected = false;
			var actual = Ex1.Equals(Ex2);

			Assert.AreEqual(expected, actual);
		}

		/// <summary>
		/// Тестирование метода Equals()
		/// </summary>
		[TestMethod]
		public void TestingEqualsSumma()
		{
			Examenation Ex1 = new Examenation("Даниил", "Широкин", "Евгеньевич", 7);
			Examenation Ex2 = new Examenation("Даниил", "Широкин", "Евгеньевич", 8);
			Examenation res = new Examenation("Даниил", "Широкин", "Евгеньевич", 7.5);
			Examenation Sum = Ex1 + Ex2;
			bool expected = true;
			var actual = Sum.Equals(res);

			Assert.AreEqual(expected, actual);
		}

		/// <summary>
		/// Тестирование метода GetHashCode()
		/// </summary>
		[TestMethod]
		public void TestingGetHashCode()
		{
			Examenation Ex = new Examenation("Даниил", "Широкин", "Евгеньевич", 8);

			var expected = 8;
			var actual = Ex.GetHashCode();

			Assert.AreEqual(expected, actual);
		}

	}
}
