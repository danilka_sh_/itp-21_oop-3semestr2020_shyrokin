﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen
{
	/// <summary>
	/// Публичный класс для отображения меню
	/// </summary>
	public class Menu
	{
		/// <summary>
		/// Метод отображения меню на экран
		/// </summary>
		public void Show()
		{
			string name, surname, patronymic;
			double rating;

			Console.WriteLine("Введите фамилию студента: ");
			surname = Console.ReadLine();
			Console.WriteLine("Введите имя студента: ");
			name = Console.ReadLine();
			Console.WriteLine("Введите отчество студента: ");
			patronymic = Console.ReadLine();

			Console.WriteLine("Введите оценку студента за экзамен: ");
			rating = Convert.ToDouble(Console.ReadLine());

			Examenation examen = new Examenation(name, surname, patronymic, rating);

			string user = "";
			while (user != "3")
			{
				Console.WriteLine("\n\tМеню");
				Console.WriteLine("1 - Вывод информации о студенте");
				Console.WriteLine("2 - Добавление еще одного экзамена и средняя оценка, между экзаменами");
				Console.WriteLine("3 - Выход");
				Console.WriteLine("\nВыберите пункт меню");

				user = Console.ReadLine();

				switch (user)
				{
					case "1":
						Console.WriteLine(examen.ToString());
						break;

					case "2":
						double other_rating;
						Console.WriteLine("Введите оценку за второй экзамен");
						other_rating = Convert.ToDouble(Console.ReadLine());
						Examenation examen_2 = new Examenation(name, surname, patronymic, other_rating);

						Console.WriteLine("\n1-й экзамен: \n" + examen.ToString());
						Console.WriteLine("\n2-й экзамен: \n" + examen_2.ToString());
						Examenation result = examen + examen_2;
						Console.WriteLine("\nСредняя оценка за экзамен: \n" + result.ToString());
						break;

					case "3":
						Console.WriteLine("\nДо свидания!");
						break;

					default:
						Console.WriteLine("\nТакого пункта в меню нету\n");
						break;
				}
			}

			Console.ReadKey();
		}
	}
}
