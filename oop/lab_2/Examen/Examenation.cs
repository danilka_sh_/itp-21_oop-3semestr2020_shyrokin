﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen
{
    /// <summary>
    /// Публичный класс для создания типа студент, содержащий информацию о экзамене
    /// </summary>
    public class Examenation
    {
        /// <summary>
        /// Имя студента
        /// </summary>
        private string name;
        /// <summary>
        /// Фамилия студента
        /// </summary>
        private string surname;
        /// <summary>
        /// Отчество студента
        /// </summary>
        private string patronymic;
        /// <summary>
        /// Оценка студента за экзамен
        /// </summary>
        private double rating;

        /// <summary>
        /// Инициализация ФИО студента и его оценки за экзамен
        /// </summary>
        /// <param name="name">Имя</param>
        /// <param name="surname">Фамилия</param>
        /// <param name="patronymic">Отчество</param>
        /// <param name="rating">Оценка</param>
        public Examenation(string name, string surname, string patronymic, double rating)
        {
            this.name = name;
            this.surname = surname;
            this.patronymic = patronymic;
            this.rating = rating;
        }

        /// <summary>
        /// Перегрузка оператора '+' для двух объектов 
        /// </summary>
        /// <param name="S1">1-й объект</param>
        /// <param name="S2">2-й объект</param>
        /// <returns>Средняя оценка за 2 экзамена</returns>
        public static Examenation operator +(Examenation S1, Examenation S2)
        {
            if (S1 == null || S2 == null)
                throw new Exception("Студента не существует");

            string name, surname, patronynic;
            double rating;

            name = S1.name;
            surname = S1.surname;
            patronynic = S1.patronymic;

            rating = (S1.rating + S2.rating) / 2;
            Examenation middle_s = new Examenation(name, surname, patronynic, rating);
            return middle_s;
        }

        /// <summary>
        /// Перегрузка оператора '-' для двух объектов 
        /// </summary>
        /// <param name="S1">1-й объект</param>
        /// <param name="S2">2-й объект</param>
        /// <returns>Средняя оценка за 2 экзамена</returns>
        public static Examenation operator -(Examenation S1, Examenation S2)
        {
            if (S1 == null || S2 == null)
                throw new Exception("Студента не существует");

            string name, surname, patronynic;
            double rating;

            name = S1.name;
            surname = S1.surname;
            patronynic = S1.patronymic;

            rating = (S1.rating + S2.rating) / 2;
            Examenation middle_s = new Examenation(name, surname, patronynic, rating);
            return middle_s;
        }

        /// <summary>
        /// Перегрузка метода ToString
        /// </summary>
        /// <returns>Строка с информацией о экзамене студента</returns>
		public override string ToString()
        {
            string result;
            result = String.Format("ФИО студента: {0} {1} {2}\n", surname, name, patronymic);
            result += String.Format("Средняя оценка за все экзамены = {0}", Convert.ToString(rating));
            return result;
        }

        /// <summary>
        /// Перегрузка метода Equals для проверки равенства двух объектов
        /// </summary>
        /// <param name="obj">объект</param>
        /// <returns>true - если объекты одинаковые, false - если разные</returns>
		public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            if (GetType() != obj.GetType())
                return false;
            Examenation ex = (Examenation)obj;
            if (rating != ex.rating)
                return false;
            if (name == ex.name && surname == ex.surname && patronymic == ex.patronymic)
                return true;
            return false;
        }

        /// <summary>
        /// Хеш-функция
        /// </summary>
        /// <returns>Хеш-код</returns>
		public override int GetHashCode()
        {
            return (int)rating;
        }

    }
}
