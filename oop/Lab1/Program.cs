﻿using System;
using ShowMenu;

namespace Library_for_figure
{
    /// <summary>
    /// Начало работы программы
    /// </summary>
    class Program
    {
        /// <summary>
        /// Ввод интервала
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            double left, right;
            Console.WriteLine("Ввод интервала для тангенса");
            Console.WriteLine("Введите значение начала интервала: ");
            left = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите значение конца интервала: ");
            right = Convert.ToDouble(Console.ReadLine());

            ShowMenu menu = new ShowMenu();
            menu.Start(left, right);
        }
    }
}
