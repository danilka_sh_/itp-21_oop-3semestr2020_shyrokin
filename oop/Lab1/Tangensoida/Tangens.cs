﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Tangensoida
{
    /// <summary>
    /// Класс для вычислений
    /// </summary>
    public class Tangens
    {
        /// <summary>
        /// Начало интервала 
        /// </summary>
        private double x1;
        /// <summary>
        /// Конец интервала 
        /// </summary>
        private double x2;
        /// <summary>
        /// Инициализация интервала
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>

        public Tangens(double left, double right)
        {
            x1 = left;
            x2 = right;
        }
        /// <summary>
        /// Проверка на существования фигуры
        /// </summary>
        /// <returns> True,если существует, </returns>

        public bool possible()
        {
            if (Math.Abs(Math.Abs(x2) - Math.Abs(x1)) <= Math.PI / 2)
            {
                if (Math.Tan(x1) * Math.Tan(x2) >= 0)
                    return true;
            }
            return false;
        }
        /// <summary>
        /// вычисление длины основания криволинейной трапеции
        /// </summary>
        /// <returns>длина основания криволинейной трапеции</returns>
        public double basis()
        {
            return Math.Abs(Math.Abs(x2) - Math.Abs(x1));
        }
        /// <summary>
        /// вычисление длины левой стороны криволинейной трапеции
        /// </summary>
        /// <returns>длина левой стороны криволинейной трапеции</returns>
        public double left_side()
        {
            return Math.Abs(Math.Tan(x1));
        }
        /// <summary>
        /// вычисление длины правой стороны криволинейной трапеции
        /// </summary>
        /// <returns>длина правой стороны криволинейной трапецц</returns>
        public double right_side()
        {
            return Math.Abs(Math.Tan(x2));
        }
        /// <summary>
        /// вычисление длины дуги криволинейной трапеции
        /// </summary>
        /// <returns>длина дуги криволиненой трапеции</returns>
        public double duga()
        {
            double x = x2 - x1;
            double y = Math.Abs(Math.Tan(x2)) - Math.Abs(Math.Tan(x1));
            Console.WriteLine(Math.Sqrt(x * x + y * y));
            return Math.Sqrt(x * x + y * y);
        }
        /// <summary>
        /// вычисление периметра криволинейной трапеции
        /// </summary>
        /// <returns>периметр трапеции</returns>
        public double perimetr()
        {
            return Math.Round(basis() + left_side() + right_side() + duga(), 2);
        }
        /// <summary>
        /// вычисление площади криволинейной трапеции
        /// </summary>
        /// <returns>площадь крив трапеции</returns>
        public double square()
        {
            double y_start = Math.Tan(x1);
            double i = x1;
            double S = 0;
            double step = ((x2 - x1) / 1000);
            while (i < x2)
            {
                S += step * y_start;
                i += step;
                y_start = Math.Tan(i);
            }
            return S;
        }
        /// <summary>
        /// проверка на существование криволинейной трапеции
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns>true - существует, иначе существует</returns>
        public bool have(double x, double y)
        {
            if (x >= x1 && x <= x2)
                if (Math.Tan(x) <= 0 && y <= 0 && y >= Math.Tan(x) || Math.Tan(x) >= 0 && y >= 0 && y <= Math.Tan(x))
                    return true;
            return false;
        }

    }
}
}
