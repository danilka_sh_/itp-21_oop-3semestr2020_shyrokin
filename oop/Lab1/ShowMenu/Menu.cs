﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tangensoida;

namespace ShowMenu
{
    /// <summary>
    /// Класс для выведения меню
    /// </summary>
    class ShowMenu
    {
        /// <summary>
        /// метод ,выводящий на экран меню и запрашивающий действие
        /// </summary>
        /// <param name="left">начало интервала на оси OX</param>
        /// <param name="right">конец интервала на оси OX</param>
        public void Start(double left, double right)
        {

            if (left > right)
            {
                double x = left;
                left = right;
                right = x;
            }

            Tangens tangens = new Tangens(left, right);

            int pun = 0;
            while (pun != 5)
            {
                Console.WriteLine("----- Меню -----");
                Console.WriteLine("1 - проверка на существование фигуры");
                Console.WriteLine("2 - периметр");
                Console.WriteLine("3 - площадь");
                Console.WriteLine("4 - проверка принадлежности точки");
                Console.WriteLine("5 - выход");

                pun = Convert.ToInt32(Console.ReadLine());

                switch (pun)
                {
                    case 1:
                        Console.WriteLine("");
                        if (tangens.possible())
                            Console.WriteLine("Такая фигура существует!\n");
                        else
                            Console.WriteLine("Такой фигуры не существует...\n");
                        break;

                    case 2:
                        if (tangens.possible())
                            Console.WriteLine("Периметр фигуры = " + tangens.perimetr() + "\n");
                        else
                            Console.WriteLine("Фигуры на заданном промежутке существовать не может\n");
                        break;

                    case 3:
                        if (tangens.possible())
                            Console.WriteLine("Площадь фигуры = " + tangens.square() + "\n");
                        else
                            Console.WriteLine("Фигуры на заданном промежутке существовать не может\n");
                        break;

                    case 4:
                        if (tangens.possible())
                        {
                            Console.WriteLine("Введите координату x: ");
                            double x = Convert.ToDouble(Console.ReadLine());
                            Console.WriteLine("Введите координату y: ");
                            double y = Convert.ToDouble(Console.ReadLine());

                            if (tangens.have(x, y))
                                Console.WriteLine("Точка принадлежит фигуре\n");
                            else
                                Console.WriteLine("Точка не принадлежит фигуре\n");
                        }
                        else
                            Console.WriteLine("Фигуры на заданном промежутке существовать не может\n");
                        break;

                    default:
                        Console.WriteLine("Такого пункта меню нету, попробуйте еще раз! ");
                        break;
                }
            }
            Console.WriteLine("Завершаем!");
            Console.ReadKey();
        }
    }
}
