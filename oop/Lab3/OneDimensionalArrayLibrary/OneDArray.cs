﻿using System;

namespace OneDimensionalArrayLibrary
{
    /// <summary>
    /// Класс OneDArray.
    /// Содержит методы работы с массивами.
    /// </summary>
    public class OneDArray
    {
        /// <summary>
        /// Свойство для определение массива
        /// </summary>
        public int ArrayLength { get; private set; }
        private int[] Array;
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="length">An int varible. The length of array.</param>
        public OneDArray(int length)
        {
            ArrayLength = length;
            Array = new int[ArrayLength];
        }
        /// <summary>
        /// Индексатор.
        /// </summary>
        /// <param name="index">></param>
        /// <returns> </returns>
        public int this[int index]
        {
            get
            {
                return Array[index];
            }
            set
            {
                Array[index] = value;
            }
        }
        /// <summary>
        /// Этот метод вводит массив.</summary>
        /// <param name="Array"></param>
        public void InputArray(int[] Array)
        {
            this.Array = Array;
        }
        /// <summary>
        /// Этот метод выводит массив.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string answer = "";
            for (int i = 0; i < ArrayLength; i++)
            {
                answer += i + ". " + Array[i] + "\n";
            }
            return answer;
        }
        /// <summary>
        /// Этот метод проверяет массив на наличие нулей.
        /// </summary>
        /// <returns>Проверка,имеются нули</returns>
        public bool Checking()
        {
            for (int i = 0; i < Array.Length; i++)
            {
                if (Array[i] == 0)
                {
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// Метод, учитывающий среднее арифметическое значение.
        /// </summary>
        /// <returns>Среднее значение.</returns>
        public int AverageNumber()
        {
            int averageSum = 0;
            for (int i = 0; i < ArrayLength; i++)
            {
                averageSum += Array[i];
            }
            averageSum /= ArrayLength;
            return averageSum;
        }
        /// <summary>
        /// Метод, который изменяет массив при определенных условиях.
        /// </summary>
        /// <param name="averageNumber"></param>
        public void ChangeArray(int averageNumber)
        {
            for (int i = 0; i < ArrayLength; i++)
            {
                if (Array[i] == 0)
                {
                    Array[i] = averageNumber;
                }
            }
        }
        /// <summary>
        /// Этот метод добавляет число в массив.
        /// </summary>
        /// <param name="k"></param>
        /// <returns>Массив с новыми значениями.</returns>
        ///  public static Student operator -(Student c1, Student c2)
        public static OneDArray operator +(OneDArray array, int k)
        {
            for (int i = 0; i < array.ArrayLength; i++)
            {
                array[i] = array[i] + k;
            }
            return array;
        }
        public static OneDArray operator +(int k, OneDArray array)
        {
            for (int i = 0; i < array.ArrayLength; i++)
            {
                array[i] = array[i] + k;
            }
            return array;
        }
        /// <summary>
        /// Этот метод проверяет наличие положительных значений в массиве и добавляет их количество.
        /// </summary>
        /// <param name="data"></param>
        /// <returns>Количество положительного числа.</returns>
        public static int PositiveNumber(params OneDArray[] data)
        {
            int quantity = 0;
            foreach (OneDArray Array in data)
            {
                for (int i = 0; i < Array.ArrayLength; i++)
                {
                    if (Array[i] > 0)
                    {
                        quantity++;
                    }
                }
            }
            return quantity;
        }
    }
}
