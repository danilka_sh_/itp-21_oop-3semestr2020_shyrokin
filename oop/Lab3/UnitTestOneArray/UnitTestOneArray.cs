﻿using System;
using OneDimensionalArrayLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestArray
{
    /// <summary>
    /// Класс, который тестирует программу.
    /// </summary>
    [TestClass]
    public class UnitTestOneArray
    {
        /// <summary>
        /// Данный тест методом проверки нулей корректно работает со значениями 0, 1, 2 в массиве.
        /// </summary>
        [TestMethod]
        public void Checking1()
        {
            int[] array = { 0, 1, 2 };
            OneDArray x = new OneDArray(3);
            x.InputArray(array);
            bool expected = false;
            bool actual = x.Checking();
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Данный тестовый метод проверяет нули, корректно работает со значениями 0,-2, 8 в массиве.
        /// </summary>
        [TestMethod]
        public void Checking2()
        {
            int[] array = { 1, -2, 8 };
            OneDArray x = new OneDArray(3);
            x.InputArray(array);
            bool expected = true;
            bool actual = x.Checking();
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Данный тестовый метод проверяет нули, корректно работает со значениями 0, 1, 2 в массиве.
        /// </summary>
        [TestMethod]
        public void Checking3()
        {
            int[] array = { 1, -2, 8, 0, 0 };
            OneDArray x = new OneDArray(5);
            x.InputArray(array);
            bool expected = false;
            bool actual = x.Checking();
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Данный тест проверяет, правильно ли работает метод положительных чисел со значениями 0, -2, 8 в arrayA и 3, -8, 7 в arrayB.
        /// </summary>
        [TestMethod]
        public void PositiveNumber()
        {
            int[] arrayA = { 0, -2, 8 };
            OneDArray x1 = new OneDArray(3);
            x1.InputArray(arrayA);
            int expected = 1;
            int actual = OneDArray.PositiveNumber(x1);
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Данный тест проверяет, правильно ли работает метод положительных чисел со значениями 0, -2, 8 в массиве A и -3, -8, 0 в массиве B.
        /// </summary>
        [TestMethod]
        public void PositiveNumber2()
        {
            int[] arrayA = { 0, -2, -8 };
            OneDArray x1 = new OneDArray(3);
            x1.InputArray(arrayA);
            int expected = 0;
            int actual = OneDArray.PositiveNumber(x1);
            Assert.AreEqual(expected, actual);
        }
        /// <summary>
        /// Данный тест проверяет, правильно ли работает метод положительных чисел со значениями 1, 2, 8 в массиве A и 3, 8, 9, 8, 5, 6 в arrayB.
        /// </summary>
        [TestMethod]
        public void PositiveNumber3()
        {
            int[] arrayB = { 3, 8, 9, 8, 5 };
            OneDArray x2 = new OneDArray(5);
            x2.InputArray(arrayB);
            int expected = 5;
            int actual = OneDArray.PositiveNumber(x2);
            Assert.AreEqual(expected, actual);
        }
    }
}
