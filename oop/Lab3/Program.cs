﻿using System;
using OneDimensionalArrayLibrary;

namespace OneDimentionalArray
{
    class Program
    {
        static void Main(string[] args)
        {
            int menu;
            int lengthA = 0;
            int lengthB = 0;
            int lengthC = 0;
            OneDArray A = null;
            OneDArray B = null;
            OneDArray C = null;
            int[] arrayA = null;
            int[] arrayB = null;
            int[] arrayC = null;
            do
            {
                Console.WriteLine("                              ");
                Console.WriteLine("*************MENU*************");
                Console.WriteLine("1. Ввод массива");
                Console.WriteLine("2. Вывод массива");
                Console.WriteLine("3. Проверка");
                Console.WriteLine("4. Общее количество положительных элементов в массиве 5+A и C+2");
                Console.WriteLine("5. Общее количество положительных элементов в массиве 2+B, A и C+4");
                Console.WriteLine("6. Если B[] есть нули");
                Console.WriteLine("7. Выход");
                Console.WriteLine("******************************");
                Console.Write("Выбирете пункт меню: ");
                Int32.TryParse(Console.ReadLine(), out menu);
                switch (menu)
                {
                    case 1:
                        Console.Write("Введите длину массива A: ");
                        Int32.TryParse(Console.ReadLine(), out lengthA);
                        A = new OneDArray(lengthA);
                        arrayA = new int[lengthA];
                        for (int i = 0; i < lengthA; i++)
                        {
                            Int32.TryParse(Console.ReadLine(), out arrayA[i]);
                        }
                        A.InputArray(arrayA);
                        Console.Write("Введите длину массива B: ");
                        Int32.TryParse(Console.ReadLine(), out lengthB);
                        B = new OneDArray(lengthB);
                        arrayB = new int[lengthB];
                        for (int i = 0; i < lengthB; i++)
                        {
                            Int32.TryParse(Console.ReadLine(), out arrayB[i]);
                        }
                        B.InputArray(arrayB);
                        Console.Write("Введите длину массиваC: ");
                        Int32.TryParse(Console.ReadLine(), out lengthC);
                        C = new OneDArray(lengthC);
                        arrayC = new int[lengthC];
                        for (int i = 0; i < lengthC; i++)
                        {
                            Int32.TryParse(Console.ReadLine(), out arrayC[i]);
                        }
                        C.InputArray(arrayC);
                        break;
                    case 2:
                        Console.WriteLine("Вывод массива A:");
                        Console.WriteLine(A.ToString());
                        Console.WriteLine("Вывод массива B:");
                        Console.WriteLine(B.ToString());
                        Console.WriteLine("Вывод массива C:");
                        Console.WriteLine(C.ToString());
                        break;
                    case 3:
                        if (A.Checking())
                        {
                            Console.WriteLine("В массиве A нет нулей.");
                        }
                        else
                        {
                            Console.WriteLine("В массиве A есть нули.");
                        }
                        if (B.Checking())
                        {
                            Console.WriteLine("В массиве B нет нулей.");
                        }
                        else
                        {
                            Console.WriteLine("В массиве B есть нули.");
                        }
                        if (C.Checking())
                        {
                            Console.WriteLine("В массиве C нет нулей.");
                        }
                        else
                        {
                            Console.WriteLine("В массиве C есть нули.");
                        }
                        break;
                    case 4:
                        int quantity;
                        quantity = OneDArray.PositiveNumber(5 + A, C + 2);
                        Console.WriteLine(A.ToString());
                        Console.WriteLine(C.ToString());
                        Console.WriteLine(quantity);
                        break;
                    case 5:
                        int quant;
                        quant = OneDArray.PositiveNumber(A, 2 + B, C + 4);
                        Console.WriteLine(A.ToString());
                        Console.WriteLine(B.ToString());
                        Console.WriteLine(C.ToString());
                        Console.WriteLine(quant);
                        break;
                    case 6:
                        if ((A.Checking()) && (OneDArray.PositiveNumber(A) > 3) && !(B.Checking()))
                        {
                            B.ChangeArray(A.AverageNumber());
                            Console.WriteLine(B.ToString());
                        }
                        else
                        {
                            Console.WriteLine("Нет нулей");
                        }
                        break;
                }
            } while (menu != 7);
        }
    }
}
