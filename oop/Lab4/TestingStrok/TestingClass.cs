﻿using System;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LibraryStroks;

namespace TestingStrok
{
	/// <summary>
	/// тестовый класс
	/// </summary>
	[TestClass]
	public class TestingClass
	{
		/// <summary>
		/// тестирования метода, удаляющего вподряд идущие одинаковые слова
		/// </summary>
		[TestMethod]
		public void TestingDeleteRepeatWord()
		{
			string user = "привет привет привет пока пока. ";
			string expect = "привет пока ";

			ProcessingStrok pr = new ProcessingStrok(user);
			string actual = pr.DeleteRepeatWord();

			Assert.AreEqual(expect, actual);
		}

		/// <summary>
		/// тестирование метода, удаляющего вподряд идущие одинаковые слова с несколькими предложениями
		/// </summary>
		[TestMethod]
		public void TestingDeleteRepeatWord_2()
		{
			string user = "привет привет привет пока пока. еще одно одно предложение. ";
			string expect = "привет пока еще одно предложение. ";

			ProcessingStrok pr = new ProcessingStrok(user);
			string actual = pr.DeleteRepeatWord();

			Assert.AreEqual(expect, actual);
		}

		/// <summary>
		/// тестирование метода, проверяющего правильность введенных строк (когда все верно)
		/// </summary>
		[TestMethod]
		public void TestingCheck()
		{
			string user = "192.168.1.0 12.12.2020 12-10. 192.168.1.0 12.12.2020 12-10. 192.168.1.1 12.12.2020 12-10.";
			string[] mas = user.Split(new string[1] { ". " }, StringSplitOptions.RemoveEmptyEntries);
			bool expect = true;

			ProcessingStrok pr = new ProcessingStrok(user);
			bool actual = pr.CheckStrok(mas);

			Assert.AreEqual(expect, actual);
		}

		/// <summary>
		/// тестирование метода, проверяющего правильность введенных строк (когда не верно время)
		/// </summary>
		[TestMethod]
		public void TestingCheckWithFalseStrok()
		{
			string user = "192.10.1.0 12.30.2020 12-99";
			string[] mas = user.Split(new string[1] { ". " }, StringSplitOptions.RemoveEmptyEntries);
			bool expect = false;

			ProcessingStrok pr = new ProcessingStrok(user);
			bool actual = pr.CheckStrok(mas);

			Assert.AreEqual(expect, actual);
		}

		/// <summary>
		/// тестирование метода, проверяющего правильность введенных строк (когда не верна вся строка)
		/// </summary>
		[TestMethod]
		public void TestingCheckWithOtherStrok()
		{
			string user = "строка, не имеющая ничего подходящего";
			string[] mas = user.Split(new string[1] { ". " }, StringSplitOptions.RemoveEmptyEntries);
			bool expect = false;

			ProcessingStrok pr = new ProcessingStrok(user);
			bool actual = pr.CheckStrok(mas);

			Assert.AreEqual(expect, actual);
		}

		/// <summary>
		/// тестирование метода, записывающая в stringbuilder только неповторяющиеся адреса 
		/// </summary>
		[TestMethod]
		public void TestingAddNeedStrok()
		{
			string user = "192.168.1.0 12.12.2020 12-10. 192.168.1.0 12.12.2020 12-10. 192.168.1.1 12.12.2020 12-10.";
			StringBuilder expect = new StringBuilder();
			expect.AppendLine("192.168.1.1 12.12.2020");

			ProcessingStrok pr = new ProcessingStrok("");
			StringBuilder actual = pr.MakeStringBuilder(user);

			Assert.AreEqual(expect.ToString(), actual.ToString());
		}

		/// <summary>
		/// тестирование метода, записывающая в stringbuilder только неповторяющиеся адреса 
		/// </summary>
		[TestMethod]
		public void TestingAddNeedStrok_2()
		{
			string user = "192.168.1.0 12.12.2020 12-10. 192.168.7.2 10.12.2021 12-10. 192.168.1.1 12.12.2019 12-10.";
			StringBuilder expect = new StringBuilder();
			expect.AppendLine("192.168.1.0 12.12.2020");
			expect.AppendLine("192.168.7.2 10.12.2021");
			expect.AppendLine("192.168.1.1 12.12.2019");

			ProcessingStrok pr = new ProcessingStrok("");
			StringBuilder actual = pr.MakeStringBuilder(user);

			Assert.AreEqual(expect.ToString(), actual.ToString());
		}

		/// <summary>
		/// тестирование метода ToString() 
		/// </summary>
		[TestMethod]
		public void TestingToString()
		{
			string user = "привет, какая-то строка. добавлены знаки пунктуации. для разбития.";
			ProcessingStrok pr = new ProcessingStrok(user);
			string actual = pr.ToString();

			Assert.AreEqual(user, actual);
		}

		/// <summary>
		/// тестирование метода ToEquals() 
		/// </summary>
		[TestMethod]
		public void TestingEquals()
		{
			string user = "привет, какая-то строка. добавлены знаки пунктуации. для разбития.";
			ProcessingStrok pr1 = new ProcessingStrok(user);
			ProcessingStrok pr2 = new ProcessingStrok(user);

			bool expect = true;
			bool actual = pr1.Equals(pr2);

			Assert.AreEqual(expect, actual);
		}

		/// <summary>
		/// тестирование метода GetHashCode()
		/// </summary>
		[TestMethod]
		public void TestingGetHashCode()
		{
			string user = "привет, какая-то строка. добавлены знаки пунктуации. для разбития.";
			ProcessingStrok pr1 = new ProcessingStrok(user);

			int expect = 3;
			int actual = pr1.GetHashCode();

			Assert.AreEqual(expect, actual);
		}

	}
}
