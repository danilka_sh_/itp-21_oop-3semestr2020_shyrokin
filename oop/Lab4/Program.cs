﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibraryStroks;

namespace Lab4
{
	/// <summary>
	/// начальный класс
	/// </summary>
	class Program
	{
		/// <summary>
		/// точка входа в программу
		/// </summary>
		/// <param name="args">аргументы метода main</param>
		static void Main(string[] args)
		{
			Menu menu = new Menu();
			menu.ShowMenu();
		}
	}
}
