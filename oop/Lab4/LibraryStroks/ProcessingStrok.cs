﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace LibraryStroks
{
    /// <summary>
    /// Класс для обработки строк
    /// </summary>
    public class ProcessingStrok
    {
        /// <summary>
        /// массив со строками
        /// </summary>
        private string[] mas_strok;

        /// <summary>
        /// инициализация массива строка
        /// </summary>
        /// <param name="strok">строка для разбития в массив</param>
        public ProcessingStrok(string strok)
        {
            string[] user_mas = strok.Split(new string[] { ". ", "! ", "? " },
                StringSplitOptions.RemoveEmptyEntries);
            mas_strok = new string[user_mas.Length];
            for (int i = 0; i < user_mas.Length; i++)
                mas_strok[i] = user_mas[i];
        }

        /// <summary>
        /// Удаление из строки повторяющихся слов, идущих вподряд
        /// </summary>
        /// <returns>строка с удаленными словами</returns>
        public string DeleteRepeatWord()
        {
            string res = "";
            string[] words;

            for (int i = 0; i < mas_strok.Length; i++)
            {
                words = mas_strok[i].Split(' ');
                for (int j = 0; j < words.Length - 1; j++)
                {
                    res += words[j] + " ";
                    while (j < words.Length - 1 && words[j] == words[j + 1])
                        j++;
                }

                if (words.Length > 1 && words[words.Length - 1] != words[words.Length - 2])
                    res += words[words.Length - 1] + ". ";
            }

            return res;
        }

        /// <summary>
        /// создание stringbuilder без повторяющихся адресов
        /// </summary>
        /// <param name="data">строка для обработки</param>
        /// <returns>stringbuilder без повторяющихся адресов</returns>
        public StringBuilder MakeStringBuilder(string data)
        {
            StringBuilder res = new StringBuilder();
            string[] mas = data.Split(new string[1] { ". " }, StringSplitOptions.RemoveEmptyEntries);

            if (!CheckStrok(mas))
                throw new Exception("Строка имела неверный формат ввода");

            string[] words;
            string one_strok;
            bool flag;

            for (int i = 0; i < mas.Length; i++)
            {
                one_strok = "";
                flag = true;
                words = mas[i].Split(' ');

                for (int j = 0; j < mas.Length; j++)
                {
                    string[] mini_word = mas[j].Split(' ');
                    if (i != j && words[0] == mini_word[0])
                        flag = false;
                }

                if (flag)
                {
                    one_strok += words[0] + " " + words[1];
                    res.AppendLine(one_strok);
                }
            }

            return res;
        }

        /// <summary>
        /// проверка каждого предложения на верный формат ввода
        /// </summary>
        /// <param name="mas">массив строк для проверки</param>
        /// <returns>true - если все верно, иначе false</returns>
        public bool CheckStrok(string[] mas)
        {
            bool flag = true;

            Regex regex1 = new Regex(@"([0-3][0-9]\.[0-1][0-9]\.[0-9]{4})");
            Regex regex2 = new Regex(@"([0-3][0-9]/[0-1][0-9]/[0-9]{2})");
            Regex regex3 = new Regex(@"([0-3][0-9]\.[0-1][0-9]\.[0-9]{2})");
            Regex regex_ip = new Regex(@"([0-9]{3}.[0-9]{3}.[0-9]{1}.[0-9]{1})");
            Regex regex_date = new Regex(@"([0-24]{1}-[0-60]{1})");

            for (int i = 0; i < mas.Length; i++)
            {
                if (!regex1.IsMatch(mas[i]) && !regex2.IsMatch(mas[i])
                && !regex3.IsMatch(mas[i]) || !regex_ip.IsMatch(mas[i])
                || !regex_date.IsMatch(mas[i]))
                    flag = false;
            }

            return flag;
        }

        /// <summary>
        /// перегруженный метод ToString()
        /// </summary>
        /// <returns>строку, сформированную из массива строк</returns>
        public override string ToString()
        {
            return String.Join(". ", mas_strok);
        }

        /// <summary>
        /// перегруженный метод для сравнения двух объектов
        /// </summary>
        /// <param name="obj">объект для сравнения с текущим</param>
        /// <returns>true - если одинаковые, иначе false</returns>
		public override bool Equals(object obj)
        {
            if (GetType() != obj.GetType())
                return false;
            ProcessingStrok stroks = (ProcessingStrok)obj;
            if (mas_strok.Length != stroks.mas_strok.Length)
                return false;
            for (int i = 0; i < mas_strok.Length; i++)
                if (mas_strok[i] != stroks.mas_strok[i])
                    return false;
            return true;
        }

        /// <summary>
        /// хеш-функция
        /// </summary>
        /// <returns>число предложений в строке</returns>
		public override int GetHashCode()
        {
            return mas_strok.Length;
        }

    }
}
