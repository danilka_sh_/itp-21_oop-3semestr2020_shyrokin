﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryStroks
{
	/// <summary>
	/// Класс для вывода меню
	/// </summary>
	public class Menu
	{
		/// <summary>
		/// метод отображения меню
		/// </summary>
		public void ShowMenu()
		{
			Console.WriteLine("Введите строку: ");
			string user_strok = Console.ReadLine();

			ProcessingStrok processing = new ProcessingStrok(user_strok);
			string user = "";

			while (user != "4")
			{
				Console.WriteLine("\nМеню");
				Console.WriteLine("1 - Вывод исходной строки");
				Console.WriteLine("2 - Строка без идущих вподряд одинаковых слов");
				Console.WriteLine("3 - Вывести ip адреса");
				Console.WriteLine("4 - Выход");
				Console.WriteLine("\nВыберите пункт меню: ");
				user = Console.ReadLine();

				switch (user)
				{
					case "1":
						Console.WriteLine("\n" + user_strok);
						break;

					case "2":
						Console.WriteLine("\n" + processing.DeleteRepeatWord());
						break;

					case "3":
						Console.WriteLine("Введите строку: ");
						string us = Console.ReadLine();
						var S = processing.MakeStringBuilder(us);
						Console.WriteLine(S.ToString());
						break;

					case "4":
						Console.WriteLine("\nДо свидания");
						break;

					default:
						Console.WriteLine("Такого пункта в меню нету!");
						break;
				}

				Console.ReadKey();

			}

		}
	}
}
